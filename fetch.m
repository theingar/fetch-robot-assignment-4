function [] = fetch()
%% D&H Parameters
clc

% Define joint limits
qlim1 = [deg2rad(-92),deg2rad(92)];
qlim2 = [deg2rad(-72),deg2rad(87)];
qlim3 = [deg2rad(-180),deg2rad(180)];
qlim4 = [deg2rad(-129),deg2rad(129)];
qlim5 = [deg2rad(-180),deg2rad(180)];
qlim6 = [deg2rad(-127),deg2rad(127)];
qlim7 = [deg2rad(-180),deg2rad(180)];

% Define Inertia
I1 = [0.006,0,0.0002,0.0014,-0.0001,0.00061];
I2 = [0.0028,-0.021,0,0.011,0,0.0112];
I3 = [0.0019,-0.0001,0,0.0045,0,0.0047];
I4 = [0.0019,-0.0001,0,0.0045,0,0.0047];
I5 = [0.0016,-0.0003,0,0.003,0,0.0035];
I6 = [0.0018,-0.0001,0,0.0042,0,0.0042];
I7 = [0.0018,-0.0001,0,0.0042,0,0.0042];

% Define location of center of mass
r1 = [0.0081 0.0025 0.0113];
r2 = [0.1432 0.0072 -0.0001];
r3 = [0.1165 0.0014 0];
r4 = [0.1279 0.0073 0];
r5 = [0.1097 -0.0266 0];
r6 = [0.0882 0.0009 -0.0001];
r7 = [0.0095 0.0004 -0.0002];

% Define links
L1 = Link('d',0.063,'a',0.117,'alpha',-pi/2,'offset',0,'qlim',qlim1,'I',I1,'r',r1,'m',0.9087);
L2 = Link('d',0,'a',0,'alpha',pi/2,'offset',-pi/2,'qlim',qlim2,'I',I2,'r',r2,'m',2.6615);
L3 = Link('d',-0.354,'a',0,'alpha',-pi/2,'offset',0,'qlim',qlim3,'I',I3,'r',r3,'m',2.3311);
L4 = Link('d',0,'a',0,'alpha',pi/2,'offset',0,'qlim',qlim4,'I',I4,'r',r4,'m',2.1299);
L5 = Link('d',-0.3245,'a',0,'alpha',-pi/2,'offset',0,'qlim',qlim5,'I',I5,'r',r5,'m',1.6563);
L6 = Link('d',0,'a',0,'alpha',pi/2,'offset',0,'qlim',qlim6,'I',I6,'r',r6,'m',1.725);
L7 = Link('d',-0.1385,'a',0,'alpha',0,'offset',0,'qlim',qlim7,'I',I7,'r',r7,'m',0.1354);

Fetch = SerialLink([L1 L2 L3 L4 L5 L6 L7],'name','Fetch');                 %Create robot using links
q0 = zeros(1,7);                                                           %end-effector
workspace = [-2 2.5 -1.5 2.5 -1 1];                                        %Define workspace area
scale = 0.1;                                                               %size of the links
Fetch.base = transl(0,0,0)                                                 %coordinate of the robot


% Object Define
stair = transl(0.45,0.5,-0.5);                                             %coordinate of the staircase
dropoff = transl(0.45,-0.5,-0.5);                                          %coordinate of the dropoff

% PlotAndColourRobot
for linkIndex = 0:Fetch.n                                                  %ply file loaded into the link of robot
        [ faceData, vertexData, plyData{linkIndex+1} ] = plyread(['j',num2str(linkIndex),'.ply'],'tri'); 
        Fetch.faces{linkIndex+1} = faceData;
        Fetch.points{linkIndex+1} = vertexData;
end
 
axis equal;
grid on;

% Load the staircase into workspace
[f,v,data] = plyread('stair.ply','tri');                                   %Place the staircase in the workspace
objVertexCount1 = size(v,1);
midPoint1 = sum(v)/objVertexCount1;
objVerts1 = v - repmat(midPoint1,objVertexCount1,1);
objPose1 = stair;
vertexColours1 = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;
objMesh_h1 = trisurf(f,objVerts1(:,1),objVerts1(:,2), objVerts1(:,3) ...
    ,'FaceVertexCData',vertexColours1,'EdgeColor','interp','EdgeLighting','flat');
updatedPoints1 = [objPose1 * [objVerts1,ones(objVertexCount1,1)]']';  
 objMesh_h1.Vertices = updatedPoints1(:,1:3);
drawnow()
camlight;

% scale the plot
Fetch.plot3d(q0, 'workspace', workspace, 'scale', scale);


%% Task 1 : Servoing

% Create image target (points in the image plane) 
pStar = objMesh_h1.Vertices;                                               %put points on vertices of staircase

% specification of the camera and camera name
cam = CentralCamera('focal', 0.08, 'pixel', 10e-5, ...                      
    'resolution', [1024 1024], 'centre', [512 512], 'name', 'FetchCamera');

% Locate the end-effector
q = Fetch.ikcon(stair*transl(0,0,0.2));
q1 = Fetch.ikcon(staircamera);

P = objMesh_h1.Vertices;

% Mount camera to the end-effector
Tc0 = Fetch.fkine(q)*troty(pi/2);                                   
Fetch.animate(q);
drawnow()
hold on

cam.T = Tc0*troty(pi);
objMesh_h1.Vertices

% Plot the camera
p = cam.plot(P, 'Tcam', Tc0);

% camera view and plotting
cam.clf()
cam.plot(pStar, '*');                                                      %create the camera view
cam.hold(true);
cam.plot(P, 'Tcam', Tc0, 'o');                                             %create the camera view
pause(2)
cam.hold(true);
cam.plot(P);                                                               %show initial view
cam.hold(true);

% frame rate
fps = 25;

% Define values
% gain of the controler
lambda = 0.6;
% depth of the IBVS
depth = mean (P(1,:)');


% Initialise display arrays
vel_p = [];
uv_p = [];
history = [];

% Looping

% loop of the visual servoing
ksteps = 0;
 while true
        ksteps = ksteps + 1;
        
        % compute the view of the camera
        uv = cam.plot(P);
        
        % compute image plane error as a column
        e = pStar-uv;   % feature error
        e = e(:);
        Zest = [];
        
        % compute the Jacobian
        if isempty(depth)
            % exact depth from simulation (not possible in practice)
            pt = homtrans(inv(Tcam), P);
            J = cam.visjac_p(uv, pt(3,:) );
        elseif ~isempty(Zest)
            J = cam.visjac_p(uv, Zest);
        else
            J = cam.visjac_p(uv, depth );
        end

        % compute the velocity of camera in camera frame
        try
            v = lambda * pinv(J) * e;
        catch
            status = -1;
            return
        end
        fprintf('v: %.3f %.3f %.3f %.3f %.3f %.3f\n', v);

        % compute robot's Jacobian and inverse
        J2 = r.model.jacobn(q0);
        Jinv = pinv(J2);
        % get joint velocities
        qp = Jinv*v;

         
         % Maximum angular velocity cannot exceed 180 degrees/s
         ind=find(qp>pi);
         if ~isempty(ind)
             qp(ind)=pi;
         end
         ind=find(qp<-pi);
         if ~isempty(ind)
             qp(ind)=-pi;
         end

        % Update joints 
        q = q0 + (1/fps)*qp;
        r.model.animate(q');

        % Get camera location
        Tc = r.model.fkine(q);
        cam.T = Tc;

        drawnow
        
        % update the history variables
        hist.uv = uv(:);
        vel = v;
        hist.vel = vel;
        hist.e = e;
        hist.en = norm(e);
        hist.jcond = cond(J);
        hist.Tcam = Tc;
        hist.vel_p = vel;
        hist.uv_p = uv;
        hist.qp = qp;
        hist.q = q;

        history = [history hist];

         pause(1/fps)

        if ~isempty(200) && (ksteps > 200)
            break;
        end
        
        % update current joint position
        q0 = q;
 end                                                                       %loop finishes

% Show the figures
figure()            
plot_p(history,pStar,cam)
figure()
plot_camera(history)
figure()
plot_vel(history)
figure()
plot_robjointpos(history)
figure()
plot_robjointvel(history)

%% Task 2A : Slow enough to make sure that none of maximum torques is exceeded
tau_max = [33.82 131.76 76.94 66.18 29.35 25.70 7.36]';
time = 10;                                                                 % Total time to execute the motion
T1 = [[0 0.5 0; 0 0 -0.5; 0.45 0 0] [0;0.7;0];zeros(1,3) 1];               % First pose
q1 = Fetch.ikcon(T1,q0);                                                   % Inverse kinematics for 1st pose
T2 = [[0 0 -0.5;0 -0.5 0; 0.45 0 0] [0.5;0;0.6];zeros(1,3) 1];             % Second pose
q2 = Fetch.ikcon(T2,q0);                                                   % Inverse kinematics for 2nd pose

dt = 1/100;                                                                % Set control frequency at 100Hz
steps = time/dt;                                                           % No. of steps along trajectory

q = jtraj(q1,q2,steps);                                                    % Quintic polynomial profile

qd = zeros(steps,7);                                                       % Array of joint velocities
qdd = nan(steps,7);                                                        % Array of joint accelerations
tau = nan(steps,7);                                                        % Array of joint torques
mass = 2;                                                                  % Payload mass (kg)
Fetch.payload(mass,[0.1;0;0]);                                             % Set payload mass in Puma 560 model: offset 0.1m in x-direction

for i = 1:steps-1
    qdd(i,:) = (1/dt)^2 * (q(i+1,:) - q(i,:) - dt*qd(i,:));                % Calculate joint acceleration to get to next set of joint angles
    M = Fetch.inertia(q(i,:));                                             % Calculate inertia matrix at this pose
    C = Fetch.coriolis(q(i,:),qd(i,:));                                    % Calculate coriolis matrix at this pose
    g = Fetch.gravload(q(i,:));                                            % Calculate gravity vector at this pose
    tau(i,:) = (M*qdd(i,:)' + C*qd(i,:)' + g')';                           % Calculate the joint torque needed
    for j = 1:6
        if abs(tau(i,j)) > tau_max(j)                                      % Check if torque exceeds limits
            tau(i,j) = sign(tau(i,j))*tau_max(j);                          % Cap joint torque if above limits
        end
    end
    qdd(i,:) = (inv(M)*(tau(i,:)' - C*qd(i,:)' - g'))';                    % Re-calculate acceleration based on actual torque
    q(i+1,:) = q(i,:) + dt*qd(i,:) + dt^2*qdd(i,:);                        % Update joint angles based on actual acceleration
    qd(i+1,:) = qd(i,:) + dt*qdd(i,:);                                     % Update the velocity for the next pose
end

t = 0:dt:(steps-1)*dt;                                                     % Generate time vector

% Visulalisation and plotting of results

% Plot joint angles
figure(1)
for j = 1:7
    subplot(3,3,j)
    plot(t,q(:,j)','k','LineWidth',1);
    refline(0,Fetch.qlim(j,1));
    refline(0,Fetch.qlim(j,2));
    ylabel('Angle (rad)');
    box off
end

% Plot joint velocities
figure(2)
for j = 1:7
    subplot(3,3,j)
    plot(t,qd(:,j)*30/pi,'k','LineWidth',1);
    refline(0,0);
    ylabel('Velocity (RPM)');
    box off
end

% Plot joint acceleration
figure(3)
for j = 1:7
    subplot(3,3,j)
    plot(t,qdd(:,j),'k','LineWidth',1);
    ylabel('rad/s/s');
    refline(0,0)
    box off
end

% Plot joint torques
figure(4)
for j = 1:7
    subplot(3,3,j)
    plot(t,tau(:,j),'k','LineWidth',1);
    refline(0,tau_max(j));
    refline(0,-tau_max(j));
    ylabel('Nm');
    box off
end

%% Task 2B: Fast enough so arm is not overloaded
tau_max = [33.82 131.76 76.94 66.18 29.35 25.70 7.36]';
time = 10;                                                                 % Total time to execute the motion
T1 = [[0 0.5 0; 0 0 -0.5; 0.45 0 0] [0;0.7;0];zeros(1,3) 1];               % First pose
q1 = Fetch.ikcon(T1,q0);                                                   % Inverse kinematics for 1st pose
T2 = [[0 0 -0.5;0 -0.5 0; 0.45 0 0] [0.5;0;0.6];zeros(1,3) 1];             % Second pose
q2 = Fetch.ikcon(T2,q0);                                                   % Inverse kinematics for 2nd pose

dt = 1/100;                                                                % Set control frequency at 100Hz
steps = time/dt;                                                           % No. of steps along trajectory

q = jtraj(q1,q2,steps);                                                    % Quintic polynomial profile

qd = zeros(steps,7);                                                       % Array of joint velocities
qdd = nan(steps,7);                                                        % Array of joint accelerations
tau = nan(steps,7);                                                        % Array of joint torques
mass = 5;                                                                  % Payload mass (kg)
Fetch.payload(mass,[0.1;0;0]);                                             % Set payload mass in Puma 560 model: offset 0.1m in x-direction

for i = 1:steps-1
    qdd(i,:) = (1/dt)^2 * (q(i+1,:) - q(i,:) - dt*qd(i,:));                % Calculate joint acceleration to get to next set of joint angles
    M = Fetch.inertia(q(i,:));                                             % Calculate inertia matrix at this pose
    C = Fetch.coriolis(q(i,:),qd(i,:));                                    % Calculate coriolis matrix at this pose
    g = Fetch.gravload(q(i,:));                                            % Calculate gravity vector at this pose
    tau(i,:) = (M*qdd(i,:)' + C*qd(i,:)' + g')';                           % Calculate the joint torque needed
    for j = 1:6
        if abs(tau(i,j)) > tau_max(j)                                      % Check if torque exceeds limits
            tau(i,j) = sign(tau(i,j))*tau_max(j);                          % Cap joint torque if above limits
        end
    end
    qdd(i,:) = (inv(M)*(tau(i,:)' - C*qd(i,:)' - g'))';                    % Re-calculate acceleration based on actual torque
    q(i+1,:) = q(i,:) + dt*qd(i,:) + dt^2*qdd(i,:);                        % Update joint angles based on actual acceleration
    qd(i+1,:) = qd(i,:) + dt*qdd(i,:);                                     % Update the velocity for the next pose
end

t = 0:dt:(steps-1)*dt;                                                     % Generate time vector

% Visulalisation and plotting of results

% Plot joint angles
figure(1)
for j = 1:7
    subplot(3,3,j)
    plot(t,q(:,j)','k','LineWidth',1);
    refline(0,Fetch.qlim(j,1));
    refline(0,Fetch.qlim(j,2));
    ylabel('Angle (rad)');
    box off
end

% Plot joint velocities
figure(2)
for j = 1:7
    subplot(3,3,j)
    plot(t,qd(:,j)*30/pi,'k','LineWidth',1);
    refline(0,0);
    ylabel('Velocity (RPM)');
    box off
end

% Plot joint acceleration
figure(3)
for j = 1:7
    subplot(3,3,j)
    plot(t,qdd(:,j),'k','LineWidth',1);
    ylabel('rad/s/s');
    refline(0,0)
    box off
end

% Plot joint torques
figure(4)
for j = 1:7
    subplot(3,3,j)
    plot(t,tau(:,j),'k','LineWidth',1);
    refline(0,tau_max(j));
    refline(0,-tau_max(j));
    ylabel('Nm');
    box off
end
end                                                                        % End the fetch function
